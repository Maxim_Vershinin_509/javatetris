package ru.nsu.ccfit.vershinin.tetris.model;

import ru.nsu.ccfit.vershinin.tetris.observer.Observable;

import java.awt.*;
import java.util.Set;

public interface ModelGetterInfoInterface extends Observable {
    class Record {
        private String name;
        private Integer score;

        Record(String name, Integer numb) {
            this.name = name;
            this.score = numb;
        }

        public String getName() {
            return name;
        }

        public Integer getScore() {
            return score;
        }
    }

    Color[][] getField();

    int getLevel();

    Color getColorNextFigure();

    int[][] getCoordNextFigure();

    int getScore();

    boolean isPaused();

    boolean isRegisterRecord();

    Set<Record> getScoresTable();
}
