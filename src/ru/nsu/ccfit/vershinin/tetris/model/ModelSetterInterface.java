package ru.nsu.ccfit.vershinin.tetris.model;

public interface ModelSetterInterface {
    void fall();

    void rotate();

    void moveRight();

    void moveLeft();

    void down();

    void initNewGame();

    void stop();

    void setRecords(String playerName);

    void setPaused();

}
