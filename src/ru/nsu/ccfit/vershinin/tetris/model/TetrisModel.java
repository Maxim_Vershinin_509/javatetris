package ru.nsu.ccfit.vershinin.tetris.model;

import ru.nsu.ccfit.vershinin.tetris.observer.Observer;

import javax.swing.Timer;
import java.awt.*;
import java.io.*;
import java.util.*;
import java.util.List;

public class TetrisModel implements TetrisModelInterface {
    public static class Block {
        private boolean isEmpty;
        private Color color;

        Block(boolean isEmpty, Color color) {
            this.isEmpty = isEmpty;
            this.color = color;
        }

        private boolean isEmpty() { return isEmpty; }

        private Color getColor() { return color; }

        private void setEmpty(boolean empty) { isEmpty = empty; }

        private void setColor(Color color) { this.color = color; }
    }

    private TetrisFigure actualTetrisFigure;
    private TetrisFigure nextTetrisFigure;
    private int[] supCoord = new int[2];
    private Block[][] field = new Block[10][24];
    private int score = 0;
    private boolean isHigher;
    private final int time = 500;
    private Timer t;
    private boolean isPaused = false;
    private boolean isStopped = true;
    private boolean isRegisterRecord = false;
    private List<Observer> observers;
    private TreeSet<Record> scoresTable;
    private String actualPlayerName;

    private void initScoresTable() {
        try {
            BufferedReader tableRecords = new BufferedReader(new FileReader("./res/table_records"));
            String str, key = "",
                    value = "";
            while (null != (str = tableRecords.readLine())) {
                if (0 != str.length()) {
                    for (int i = 0; i < str.length(); i++) {
                        if (Character.isSpaceChar(str.charAt(i))) {
                            key = str.substring(0, i);
                            value = str.substring(i+1);
                            break;
                        }
                    }
                }
                scoresTable.add(new Record(key, Integer.valueOf(value)));
            }
            tableRecords.close();
        } catch (IOException er) {
            System.err.println(er.getLocalizedMessage());
        }
    }

    private void figureTouchGround() {
        if (isHigher()) {
            t.stop();
            isStopped = true;
            isRegisterRecord = false;
            notifyObserver(SelectorPanel.GAME_PANEL);
            notifyObserver(SelectorPanel.END_GAME_DIALOG);
        } else {
            notifyObserver(SelectorPanel.SCORE_PANEL);
            notifyObserver(SelectorPanel.LEVEL_PANEL);
            notifyObserver(SelectorPanel.GAME_PANEL);
            notifyObserver(SelectorPanel.NEXT_FIGURE_PANEL);
        }
    }

    private void clearLine() {
        int coeff = 0;
        int countFullLine = 0;
        for (int i = 0; i < 24; i++) {
            boolean isLineFull = true;
            for (int j = 0; j < 10; j++) {
                if (field[j][i].isEmpty) {
                    isLineFull = false;
                    break;
                }
            }
            if (isLineFull) {
                for (int j = 0; j < 10; j++) {
                    field[j][i].setColor(new Color(17, 15, 15));
                    field[j][i].setEmpty(true);
                }
                for (int j = i; j > 4; j--) {
                    for (int k = 0; k < 10; k++) {
                        field[k][j].setColor(field[k][j-1].getColor());
                        field[k][j].setEmpty(field[k][j-1].isEmpty());
                    }
                }
                for (int j = 0; j < 10; j++) {
                    field[j][4].setColor(new Color(17, 15, 15));
                    field[j][4].setEmpty(true);
                }
                coeff += 100;
                ++countFullLine;
            }
        }
        score += coeff*countFullLine;
    }

    private void setHigher() {
        isHigher = actualTetrisFigure.getY(actualTetrisFigure.minYIndex()) + supCoord[1] < 4;
    }

    private void initField() {
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 24; j++) {
                if (j < 4) {
                    field[i][j] = new Block(true, new Color(0x640000));
                } else {
                    field[i][j] = new Block(true, new Color(17, 15, 15));
                }
            }
        }
    }

    private void setRandomTetrisFigure() {
        if (null == actualTetrisFigure) {
            actualTetrisFigure = new TetrisFigure();
        } else {
            actualTetrisFigure = nextTetrisFigure;
        }
        nextTetrisFigure = new TetrisFigure();
        supCoord[0] = 4;
        supCoord[1] = -actualTetrisFigure.getY(actualTetrisFigure.minYIndex());
        addTetrisFigure();
    }

    private void addTetrisFigure() {
        for (int i = 0; i < 4; i++) {
            field[supCoord[0] + actualTetrisFigure.getX(i)][supCoord[1] + actualTetrisFigure.getY(i)].setColor(actualTetrisFigure.getForm().getColor());
        }
    }

    private void clearTetrisFigure() {
        for (int i = 0; i < 4; i++) {
            if (supCoord[1] + actualTetrisFigure.getY(i) < 4) {
                field[supCoord[0] + actualTetrisFigure.getX(i)][supCoord[1] + actualTetrisFigure.getY(i)].setColor(new Color(0x640000));
            } else {
                field[supCoord[0] + actualTetrisFigure.getX(i)][supCoord[1] + actualTetrisFigure.getY(i)].setColor(new Color(17, 15, 15));
            }
        }
    }

    private boolean isHigher() {
        return isHigher;
    }

    private void bottomTouch() {
        --supCoord[1];
        for (int i = 0; i < 4; i++) {
            field[supCoord[0]+actualTetrisFigure.getX(i)][supCoord[1]+actualTetrisFigure.getY(i)].setEmpty(false);
        }
        setHigher();
        addTetrisFigure();
        clearLine();
        setRandomTetrisFigure();
    }

    private boolean checkBottomTouch(int numbOfBlock) {
        return supCoord[1]+actualTetrisFigure.getY(numbOfBlock) >= 24
                || !field[supCoord[0]+actualTetrisFigure.getX(numbOfBlock)][supCoord[1]+actualTetrisFigure.getY(numbOfBlock)].isEmpty();
    }

    @Override
    public void removeObserver(Observer o) {
        observers.remove(o);
    }

    @Override
    public void notifyObserver(SelectorPanel selectorPanel) {
        for (Observer o: observers) {
            o.update(selectorPanel);
        }
    }

    @Override
    public void registerObserver(Observer o) {
        observers.add(o);
    }

    @Override
    public int getScore() { return score; }

    @Override
    public int[][] getCoordNextFigure() {
        return nextTetrisFigure.getCoord();
    }

    @Override
    public Color getColorNextFigure() {
        return nextTetrisFigure.getForm().getColor();
    }

    @Override
    public Color[][] getField() {
        Color[][] colorField = new Color[field.length][field[0].length];
        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j <field[0].length; j++) {
                colorField[i][j] = field[i][j].getColor();
            }
        }
        return colorField;
    }

    @Override
    public TreeSet<Record> getScoresTable() {
        return scoresTable;
    }

    @Override
    public boolean isPaused() {
        return isPaused;
    }

    @Override
    public int getLevel() {
        return (getScore()/1000 + 1);
    }

    @Override
    public boolean isRegisterRecord() {
        return isRegisterRecord;
    }

    @Override
    public void down() {
        if (isStopped || isPaused) return;
        boolean canDown = true;
        clearTetrisFigure();
        ++supCoord[1];
        for (int i = 0; i < 4; i++) {
            if (checkBottomTouch(i)) {
                canDown = false;
                break;
            }
        }
        if (!canDown) {
            bottomTouch();
        } else {
            addTetrisFigure();
        }
        figureTouchGround();
    }

    @Override
    public void moveLeft() {
        if (isStopped || isPaused) return;
        boolean isLeftMoveable = true;
        for (int i = 0; i < 4; i++) {
            if (supCoord[0]+actualTetrisFigure.getX(i)-1 < 0
            || !field[supCoord[0]+actualTetrisFigure.getX(i)-1][supCoord[1]+actualTetrisFigure.getY(i)].isEmpty()) {
                isLeftMoveable = false;
                break;
            }
        }
        if (isLeftMoveable) {
            clearTetrisFigure();
            --supCoord[0];
            addTetrisFigure();
        }
        notifyObserver(SelectorPanel.GAME_PANEL);
    }

    @Override
    public void moveRight() {
        if (isStopped || isPaused) return;
        boolean isRightMoveable = true;
        for (int i = 0; i < 4; i++) {
            if (supCoord[0]+actualTetrisFigure.getX(i)+1 >= 10
            || !field[supCoord[0]+actualTetrisFigure.getX(i)+1][supCoord[1]+actualTetrisFigure.getY(i)].isEmpty()) {
                isRightMoveable = false;
                break;
            }
        }
        if (isRightMoveable) {
            clearTetrisFigure();
            ++supCoord[0];
            addTetrisFigure();
        }
        notifyObserver(SelectorPanel.GAME_PANEL);
    }

    @Override
    public void rotate() {
        if (isStopped || isPaused) return;
        boolean isRotable = true;
        for (int i = 0; i < 4; i++) {
            if (supCoord[0] - actualTetrisFigure.getY(i) >= 10
            || supCoord[1] + actualTetrisFigure.getX(i) >= 24
            || supCoord[0] - actualTetrisFigure.getY(i) < 0
            || supCoord[1] + actualTetrisFigure.getX(i) < 0
            || !field[supCoord[0] - actualTetrisFigure.getY(i)][supCoord[1] + actualTetrisFigure.getX(i)].isEmpty()) {
                isRotable = false;
                break;
            }
        }
        if (isRotable) {
            clearTetrisFigure();
            actualTetrisFigure.rotate();
            addTetrisFigure();
        }
        notifyObserver(SelectorPanel.GAME_PANEL);
    }

    @Override
    public void fall() {
        if (isStopped || isPaused) return;
        boolean isTouchedBottom = false;
        clearTetrisFigure();
        while (!isTouchedBottom) {
            ++supCoord[1];
            for (int i = 0; i < 4; i++) {
                if (checkBottomTouch(i)) {
                    isTouchedBottom = true;
                    break;
                }
            }
        }
        bottomTouch();
        figureTouchGround();
    }

    @Override
    public void setPaused() {
        if (isStopped) return;
        isPaused = !isPaused;
        notifyObserver(SelectorPanel.PAUSE_PANEL);
    }

    @Override
    public void initNewGame() {
        initField();
        setRandomTetrisFigure();
        isHigher = false;
        isStopped = false;
        isPaused = false;
        actualPlayerName = null;
        score = 0;
        notifyObserver(SelectorPanel.SCORE_PANEL);
        notifyObserver(SelectorPanel.LEVEL_PANEL);
        notifyObserver(SelectorPanel.GAME_PANEL);
        notifyObserver(SelectorPanel.NEXT_FIGURE_PANEL);
        notifyObserver(SelectorPanel.PAUSE_PANEL);
        notifyObserver(SelectorPanel.NEW_GAME_PANEL);
        t.start();
    }

    @Override
    public void stop() {
        if (isStopped) return;
        isStopped = true;
        t.stop();
    }

    @Override
    public void setRecords(String playerName) {
            actualPlayerName = playerName;
            isRegisterRecord = false;
            if (actualPlayerName.isEmpty()) {
                return;
            }
            isRegisterRecord = true;
            notifyObserver(SelectorPanel.END_GAME_DIALOG);
            scoresTable.add(new Record(actualPlayerName, score));
            while (scoresTable.size() > 10) {
                scoresTable.remove(scoresTable.last());
            }
            try {
                Writer tableRecords = new BufferedWriter(new FileWriter("./res/table_records"));
                for (Record r: scoresTable) {
                    tableRecords.write(r.getName() + " " + r.getScore() + "\n");
                }
                tableRecords.close();
            } catch (IOException er) {
                System.err.println(er.getLocalizedMessage());
            }
    }

    public TetrisModel() {
        observers = new ArrayList<>();
        scoresTable = new TreeSet<>((Record record, Record t1) -> {
            if (0 == record.getScore().compareTo(t1.getScore())) {
                return -record.getName().compareTo(t1.getName());
            }
            return -record.getScore().compareTo(t1.getScore());
        });
        initField();
        t = new Timer(time, (ActionEvent) -> {
            if (isPaused) return;
            t.setDelay(time - 25*(getScore()/1000));
            down();
        });
        t.stop();
        initScoresTable();
        setRandomTetrisFigure();
    }
}
