package ru.nsu.ccfit.vershinin.tetris.model;

import java.awt.*;
import java.util.Random;

public class TetrisFigure {
    public enum FigureForm {
        S_FORM(new Color(0x646401)),
        J_FORM(new Color(0x6F4713)),
        SQ_FORM(Color.YELLOW),
        T_FORM(Color.GREEN),
        L_FORM(Color.BLUE),
        Z_FORM(new Color(0x306450)),
        I_FORM(Color.PINK);

        private Color color;

        public Color getColor() {
            return color;
        }

        FigureForm(Color color) {
            this.color = color;
        }

    }

    private int[][] coord;

    private FigureForm form;

    FigureForm getForm() {
        return form;
    }

    TetrisFigure() {
        form = getRandomFigureForm();
        setCoord();
        for (int i = 0; i < new Random().nextInt(4); i++) {
            rotate();
        }
    }

    int[][] getCoord() { return coord; }

    private void setCoord() {
        switch (form) {
            case SQ_FORM:
                coord = new int[][] {{0, 0}, {-1, 0}, {0, 1}, {-1, 1}};
                break;
            case I_FORM:
                coord = new int[][] {{-2, 0}, {-1, 0}, {0, 0}, {1, 0}};
                break;
            case J_FORM:
                coord = new int[][] {{1, 0}, {0, 0}, {-1, 0}, {1, 1}};
                break;
            case L_FORM:
                coord = new int[][] {{1, 0}, {0, 0}, {-1, 0}, {-1, 1}};
                break;
            case S_FORM:
                coord = new int[][] {{0, 0}, {1, 0}, {0, 1}, {-1, 1}};
                break;
            case T_FORM:
                coord = new int[][] {{0, 0}, {-1, 0}, {1, 0}, {0, 1}};
                break;
            case Z_FORM:
                coord = new int[][] {{0, 0}, {-1, 0}, {0, 1}, {1, 1}};
                break;
        }
    }

    private static FigureForm getRandomFigureForm() {
        return FigureForm.values()[new Random().nextInt(FigureForm.values().length)];
    }

    public int minXIndex() {
        int minXIndex = 0;
        for (int i = 1; i < 4; i++) {
            if (coord[i][0] < coord[minXIndex][0]) {
                minXIndex = i;
            }
        }
        return minXIndex;
    }

    public int maxXIndex() {
        int maxXIndex = 0;
        for (int i = 1; i < 4; i++) {
            if (coord[i][0] > coord[maxXIndex][0]) {
                maxXIndex = i;
            }
        }
        return maxXIndex;
    }

    int minYIndex() {
        int minYIndex = 0;
        for (int i = 1; i < 4; i++) {
            if (coord[i][1] < coord[minYIndex][1]) {
                minYIndex = i;
            }
        }
        return minYIndex;
    }

    public int maxYIndex() {
        int maxYIndex = 0;
        for (int i = 1; i < 4; i++) {
            if (coord[i][1] > coord[maxYIndex][1]) {
                maxYIndex = i;
            }
        }
        return maxYIndex;
    }

    int getX(int index) { return coord[index][0]; }

    int getY(int index) { return coord[index][1]; }

    void rotate() {
        if (form == FigureForm.SQ_FORM) {
            return;
        }

        for (int i = 0; i < 4; i++) {
            int bufCoord = coord[i][0];
            coord[i][0] = -coord[i][1];
            coord[i][1] = bufCoord;
        }
    }
}



