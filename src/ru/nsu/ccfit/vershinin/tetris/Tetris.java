package ru.nsu.ccfit.vershinin.tetris;

import ru.nsu.ccfit.vershinin.tetris.model.TetrisModel;
import ru.nsu.ccfit.vershinin.tetris.controller.Controller;

public class Tetris {
    public static void main(String[] args) {
        Controller controller = new Controller(new TetrisModel());
    }
}
