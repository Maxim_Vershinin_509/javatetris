package ru.nsu.ccfit.vershinin.tetris.observer;

public interface Observable {
    enum SelectorPanel {
        GAME_PANEL, LEVEL_PANEL, SCORE_PANEL, PAUSE_PANEL,
        NEXT_FIGURE_PANEL, END_GAME_DIALOG, NEW_GAME_PANEL,
    }

    void registerObserver(Observer o);

    void notifyObserver(SelectorPanel panel);

    void removeObserver(Observer o);

}
