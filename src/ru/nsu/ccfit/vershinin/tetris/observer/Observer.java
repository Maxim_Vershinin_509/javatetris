package ru.nsu.ccfit.vershinin.tetris.observer;

public interface Observer {

    void update(Observable.SelectorPanel selectorPanel);

}
