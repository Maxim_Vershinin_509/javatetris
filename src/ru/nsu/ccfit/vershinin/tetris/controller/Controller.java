package ru.nsu.ccfit.vershinin.tetris.controller;

import ru.nsu.ccfit.vershinin.tetris.model.ModelSetterInterface;
import ru.nsu.ccfit.vershinin.tetris.model.TetrisModelInterface;
import ru.nsu.ccfit.vershinin.tetris.view.TetrisView;

public class Controller implements ControllerInterface {

    private ModelSetterInterface tetrisModel;
    private TetrisView tetrisView;

    @Override
    public void startGame() {
        tetrisModel.initNewGame();
    }

    @Override
    public void down() {
        tetrisModel.down();
    }

    @Override
    public void rotate() {
        tetrisModel.rotate();
    }

    @Override
    public void moveLeft() {
        tetrisModel.moveLeft();
    }

    @Override
    public void moveRight() {
        tetrisModel.moveRight();
    }

    @Override
    public void fall() {
        tetrisModel.fall();
    }

    @Override
    public void goToMenu() {
        tetrisModel.stop();
        tetrisView.goToMenu();
    }

    @Override
    public void setRecords() {
        tetrisModel.setRecords(tetrisView.getPlayerName());
    }

    @Override
    public void setPaused() {
        tetrisModel.setPaused();
    }

    public Controller(TetrisModelInterface tetrisModel) {
        this.tetrisModel = tetrisModel;
        tetrisView = new TetrisView(this, tetrisModel);
    }

}
