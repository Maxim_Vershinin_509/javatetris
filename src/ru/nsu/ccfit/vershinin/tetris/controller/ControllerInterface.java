package ru.nsu.ccfit.vershinin.tetris.controller;

public interface ControllerInterface {
    void startGame();

    void down();

    void rotate();

    void moveRight();

    void moveLeft();

    void fall();

    void goToMenu();

    void setRecords();

    void setPaused();

}
