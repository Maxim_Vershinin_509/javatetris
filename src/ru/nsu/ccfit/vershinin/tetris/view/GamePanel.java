package ru.nsu.ccfit.vershinin.tetris.view;

import javax.swing.*;
import java.awt.*;
import java.util.Arrays;

class GamePanel extends JPanel {
    private final int width = 400;
    private final int height = 960;
    private Color[][] field;
    private Color borderColor = Color.WHITE;

    void setField(Color[][] field) { this.field = field; }

    @Override
    public void paint(Graphics g) {
        Graphics2D g2D = (Graphics2D) g;
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 24; j++) {
                g2D.setColor(field[i][j]);
                g2D.fillRect(i*width/10 - 1, j*height/24 - 1, width/10, height/24);
                g2D.setColor(borderColor);
                g2D.setStroke(new BasicStroke(2));
                g2D.drawRect(i*width/10 - 1, j*height/24 - 1, width/10, height/24);
            }
        }
    }

    GamePanel() {
        field = new Color[10][24];
        for (int i = 0; i < 10; i++) {
            Arrays.fill(field[i], Color.GRAY);
        }
        this.setPreferredSize(new Dimension(width, height));
    }



}
