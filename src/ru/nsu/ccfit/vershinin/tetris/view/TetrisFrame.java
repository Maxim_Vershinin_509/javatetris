package ru.nsu.ccfit.vershinin.tetris.view;

import ru.nsu.ccfit.vershinin.tetris.model.TetrisModelInterface;

import javax.swing.*;
import java.awt.*;
import java.util.Set;

class TetrisFrame extends JFrame {
    private GamePanel gamePanel;
    private InfoAboutGamePanel infoAboutGamePanel;
    private MenuPanel menuPanel;
    private Image image = Toolkit.getDefaultToolkit().createImage("./res/back.png");
    private JLabel picLabel;
    private EndDialog endDialog;
    private JPanel aboutPanel;
    private JPanel scorePanel;
    private JPanel tablePanel;
    private JPanel tetrisPanel;
    private boolean isMenu;

    private void initGamePanel(GridBagConstraints constraints) {
        gamePanel = new GamePanel();
        constraints.gridx = GridBagConstraints.NONE;
        constraints.gridheight = 1;
        tetrisPanel.add(gamePanel, constraints);
    }

    private void initInfoAboutGamePanel(GridBagConstraints constraints) {
        infoAboutGamePanel = new InfoAboutGamePanel();
        constraints.gridx = 1;
        constraints.gridheight = 1;
        tetrisPanel.add(infoAboutGamePanel, constraints);
    }

    private void initScorePanel(GridBagConstraints constraints) {
        scorePanel = new JPanel();
        scorePanel.setLayout(null);
        scorePanel.setBackground(new Color(0, 0, 0, 0));
        tablePanel = new JPanel();
        tablePanel.setLayout(new GridLayout(10, 1, 1, 1));
        tablePanel.setBounds(100, 230, 500, 600);

        tablePanel.setBackground(new Color(0, 0, 0, 0));
        scorePanel.add(tablePanel);

        JButton backButton = new JButton("Back");
        backButton.setBounds(380, 900, 200, 30);
        backButton.setActionCommand("back score");
        backButton.addActionListener((ActionListener) -> {
            scorePanel.setVisible(false);
            menuPanel.setVisible(true);
        });

        scorePanel.add(backButton);

        constraints.gridy = 0;
        constraints.gridx = 0;
        constraints.gridwidth = 2;

        scorePanel.setPreferredSize(new Dimension(600, 960));

        tetrisPanel.add(scorePanel, constraints);
    }

    private void initAboutPanel(GridBagConstraints constraints) {
        aboutPanel = new JPanel();
        aboutPanel.setLayout(null);

        aboutPanel.setBackground(new Color(0, 0, 0, 0));

        JLabel aboutText = new JLabel("<html><center>Created by<br />Vershinin Maxim</center></html>");
        aboutText.setVisible(true);
        aboutText.setFont(new Font(null, Font.BOLD, 30));
        aboutText.setForeground(Color.WHITE);
        aboutText.setBounds(150, 400, 300, 100);
        aboutPanel.add(aboutText);

        JButton backButton = new JButton("Back");
        backButton.setBounds(390, 900, 200, 30);
        backButton.setActionCommand("back about");
        backButton.addActionListener((ActionListener) -> {
            aboutPanel.setVisible(false);
            menuPanel.setVisible(true);
        });
        aboutPanel.add(backButton);

        constraints.gridx = 0;
        constraints.gridy = 0;
        constraints.gridwidth = 2;

        aboutPanel.setPreferredSize(new Dimension(600, 960));

        tetrisPanel.add(aboutPanel, constraints);
    }

    private void initPicLabel(GridBagConstraints constraints) {
        ImageIcon imageIcon = new ImageIcon(image);
        picLabel = new JLabel(imageIcon);

        picLabel.setBackground(new Color(0, 0, 0, 0));

        picLabel.setPreferredSize(new Dimension(600, 960));

        picLabel.setBounds(0, 0, 600, 960);

        constraints.gridx = 0;
        constraints.gridy = 0;
        constraints.gridwidth = 2;

        tetrisPanel.add(picLabel, constraints);
    }

    private void initMenuPanel(GridBagConstraints constraints) {

        constraints.gridx = 0;
        constraints.gridy = 0;
        constraints.gridwidth = 2;

        tetrisPanel.add(menuPanel, constraints);
    }

    private void initTetrisFrame() {

        endDialog = new EndDialog(this);

        menuPanel = new MenuPanel();

        tetrisPanel = new JPanel();

        tetrisPanel.setLayout(new GridBagLayout());

        gamePanel = new GamePanel();
        GridBagConstraints constraints = new GridBagConstraints();

        initGamePanel(constraints);
        initInfoAboutGamePanel(constraints);
        initMenuPanel(constraints);
        initAboutPanel(constraints);
        initScorePanel(constraints);
        initPicLabel(constraints);

        add(tetrisPanel);

        scorePanel.setVisible(false);
        aboutPanel.setVisible(false);
        gamePanel.setVisible(false);
        infoAboutGamePanel.setVisible(false);
        isMenu = true;
        this.pack();
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setResizable(false);
        this.setLocationRelativeTo(null);
    }

    void updateGamePanel(Color[][] field) {
        gamePanel.setField(field);
        gamePanel.repaint();
    }

    void updateLevelPanel(int level) {
        infoAboutGamePanel.setLevelPanel(level);
        infoAboutGamePanel.repaint();
        picLabel.repaint();
    }

    void updateNextFigurePanel(int[][] nextFigureCoord, Color nextFigureColor) {
        infoAboutGamePanel.setNextFigurePanel(nextFigureCoord, nextFigureColor);
        infoAboutGamePanel.repaint();
    }

    void updatePausePanel(boolean isPaused) {
        infoAboutGamePanel.setPausePanel(isPaused);
        gamePanel.setVisible(!isPaused);
        gamePanel.repaint();
        infoAboutGamePanel.repaint();
    }

    void updateScorePanel(int score) {
        infoAboutGamePanel.setScorePanel(score);
        infoAboutGamePanel.repaint();
        picLabel.repaint();
    }

    void showAbout() {
        isMenu = true;
        menuPanel.setVisible(false);
        aboutPanel.setVisible(true);
    }

    void showHighScores(Set<TetrisModelInterface.Record> scoresTable) {
        isMenu = true;
        tablePanel.removeAll();
        for (TetrisModelInterface.Record r: scoresTable) {
            JLabel textPanel = new JLabel(r.getName() + ": " + r.getScore(), SwingConstants.LEFT);
            textPanel.setFont(new Font(null, Font.BOLD, 24));
            textPanel.setForeground(Color.WHITE);
            tablePanel.add(textPanel);
        }
        menuPanel.setVisible(false);
        scorePanel.setVisible(true);
    }

    void initGamePanels() {
        isMenu = false;
        setFocusable(true);
        gamePanel.setVisible(true);
        infoAboutGamePanel.setVisible(true);
        menuPanel.setVisible(false);
    }

    void goToMenu() {
        if (!isMenu) {
            isMenu = true;
            menuPanel.setVisible(true);
            gamePanel.setVisible(false);
            infoAboutGamePanel.setVisible(false);
        }
    }

    String getEnteredName() {
        return endDialog.getEnteredName();
    }

    void updateEndGameDialog(boolean isRegisterName) {
        endDialog.eraseTextField();
        endDialog.setVisible(!isRegisterName);
        if (isRegisterName) {
            goToMenu();
        }
    }

    @Override
    public void repaint() {
        gamePanel.repaint();
        infoAboutGamePanel.repaint();
        picLabel.repaint();
    }

    TetrisFrame(TetrisView tetrisView) {
        super("Tetris");
        initTetrisFrame();
        addKeyListener(tetrisView);
        endDialog.registerController(tetrisView);
        menuPanel.registerController(tetrisView);
    }
}
