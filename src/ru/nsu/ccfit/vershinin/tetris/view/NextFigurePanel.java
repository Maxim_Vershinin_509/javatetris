package ru.nsu.ccfit.vershinin.tetris.view;

import javax.swing.*;
import java.awt.*;
import java.util.Arrays;

class NextFigurePanel extends JPanel {
    private class MiniFieldPanel extends JPanel {
        private int minX() {
            int minX = nextFigure[0][0];
            for (int i = 1; i < 4; i++) {
                if (nextFigure[i][0] < minX) {
                    minX = nextFigure[i][0];
                }
            }
            return minX;
        }

        private int minY() {
            int minY = nextFigure[0][1];
            for (int i = 1; i < 4; i++) {
                if (nextFigure[i][1] < minY) {
                    minY = nextFigure[i][1];
                }
            }
            return minY;
        }

        @Override
        public void paint(Graphics g) {

            for (int i = 0; i < 4; i++) {
                Arrays.fill(miniField[i], new Color(17, 15, 15));
            }

            for (int i = 0; i < 4; i++) {
                miniField[nextFigure[i][0] - minX()][nextFigure[i][1] - minY()] = colorNextFigure;
            }

            Graphics2D g2D = (Graphics2D) g;
            for (int i = 0; i < 4; i++) {
                for (int j = 0; j < 4; j++) {
                    g2D.setColor(miniField[i][j]);
                    g2D.fillRect(i*40 - 1, j*40 - 1, 40, 40);
                    g2D.setStroke(new BasicStroke(2));
                    g2D.setColor(Color.WHITE);
                    g2D.drawRect(i*40, j*40, 40, 40);
                }
            }
        }
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
    }

    private Color colorNextFigure = Color.GRAY;
    private int[][] nextFigure;
    private Color[][] miniField;

    void setNextFigure(int[][] nextFigure) { this.nextFigure = nextFigure; }

    void setColorNextFigure(Color colorNextFigure) { this.colorNextFigure = colorNextFigure; }

    NextFigurePanel() {
        setLayout(new FlowLayout(FlowLayout.CENTER));

        JLabel nextFigureText = new JLabel("Next Figure:");
        nextFigureText.setPreferredSize(new Dimension(200, 40));

        nextFigureText.setHorizontalAlignment(JLabel.CENTER);
        nextFigureText.setVerticalAlignment(JLabel.BOTTOM);
        nextFigureText.setForeground(Color.WHITE);
        nextFigureText.setFont(new Font(null, Font.BOLD, 20));

        add(nextFigureText);

        MiniFieldPanel miniFieldPanel = new MiniFieldPanel();

        miniFieldPanel.setPreferredSize(new Dimension(180, 180));

        add(miniFieldPanel);

        setPreferredSize(new Dimension(200,240));
        nextFigure = new int[4][2];

        setBackground(new Color(0, 0, 0, 0));
        for (int i = 0; i < 4; i++) {
            Arrays.fill(nextFigure[i], 0);
        }
        miniField = new Color[4][4];
    }
}
