package ru.nsu.ccfit.vershinin.tetris.view;

import javax.swing.*;
import java.awt.*;

class ScorePanel extends JPanel {
    private JLabel scoreLabel;

    void setScore(int score) {
        scoreLabel.setText(Integer.toString(score));
    }

    ScorePanel() {

        setLayout(new FlowLayout(FlowLayout.CENTER));

        JLabel scoreText = new JLabel("Score:");
        scoreText.setPreferredSize(new Dimension(200, 120));

        scoreText.setHorizontalAlignment(JLabel.CENTER);
        scoreText.setVerticalAlignment(JLabel.BOTTOM);
        scoreText.setForeground(Color.WHITE);
        scoreText.setFont(new Font(null, Font.BOLD, 24));

        add(scoreText);

        scoreLabel = new JLabel(Integer.toString(0));
        scoreLabel.setPreferredSize(new Dimension(190, 120));

        scoreLabel.setHorizontalAlignment(JLabel.CENTER);
        scoreLabel.setVerticalAlignment(JLabel.NORTH);
        scoreLabel.setForeground(Color.WHITE);
        scoreLabel.setFont(new Font(null, Font.BOLD, 20));

        add(scoreLabel);

        setBackground(new Color(0, 0, 0, 0));
        
        setPreferredSize(new Dimension(200,240));
    }
}
