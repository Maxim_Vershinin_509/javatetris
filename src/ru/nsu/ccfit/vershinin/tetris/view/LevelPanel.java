package ru.nsu.ccfit.vershinin.tetris.view;

import javax.swing.*;
import java.awt.*;

class LevelPanel extends JPanel {
    private JLabel levelText;

    void setLevelText(int level) {
        levelText.setText("Level: " + level);
    }

    LevelPanel() {
        levelText = new JLabel("Level: " + 1);
        levelText.setForeground(Color.WHITE);
        levelText.setPreferredSize(new Dimension(200, 200));
        levelText.setFont(new Font(null, Font.BOLD, 30));
        levelText.setHorizontalAlignment(JLabel.CENTER);
        levelText.setVerticalAlignment(JLabel.CENTER);

        setBackground(new Color(0, 0, 0, 0));

        add(levelText);
    }
}
