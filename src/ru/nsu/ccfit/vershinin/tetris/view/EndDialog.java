package ru.nsu.ccfit.vershinin.tetris.view;

import javax.swing.*;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;
import java.awt.*;
import java.awt.event.ActionListener;

class EndDialog extends JDialog {

    private JTextField textField;
    private JButton enterButton;

    void registerController(ActionListener actionListener) {
        enterButton.addActionListener(actionListener);
    }

    String getEnteredName() {
        return textField.getText();
    }

    void eraseTextField() {
        textField.setText(null);
    }

    private void initEndDialog() {
        setLayout(null);

        JLabel textPanel = new JLabel("Enter your name:");

        textPanel.setPreferredSize(new Dimension(200, 70));

        textPanel.setBounds(10, 20, 200, 25);
        textPanel.setFont(new Font(null, Font.BOLD, 16));

        textField = new JTextField();

        textField.setBounds(30, 70, 130, 20);

        textField.setPreferredSize(new Dimension(200, 70));

        textField.setDocument(new PlainDocument() {
            @Override
            public void insertString(int offs, String str, AttributeSet a) throws BadLocationException {
                if (str == null) {
                    return;
                }
                if (getLength() + str.length() <= 10) {
                    for (int i = 0; i < str.length(); i++) {
                        if (Character.isSpaceChar(str.charAt(i))) {
                            return;
                        }
                    }
                    super.insertString(offs, str, a);
                }
            }
        });

        enterButton = new JButton("Enter");

        enterButton.setActionCommand("enter name");

        enterButton.setPreferredSize(new Dimension(200, 60));

        enterButton.setBounds(90, 125, 90, 30);

        add(enterButton);

        add(textPanel);

        add(textField);

        setPreferredSize(new Dimension(200, 200));

        pack();
        setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
        setResizable(false);
        setLocationRelativeTo(null);
    }

    EndDialog(Frame owner) {
        super(owner);
        initEndDialog();
    }
}
