package ru.nsu.ccfit.vershinin.tetris.view;

import javax.swing.*;
import java.awt.*;

class PausePanel extends JPanel {
    private JLabel pauseText;

    void setPauseText(boolean isPaused) {
        if (isPaused) {
            pauseText.setText("Pause");
            pauseText.setFont(new Font(null, Font.BOLD, 40));
        } else {
            pauseText.setText("Game");
            pauseText.setFont(new Font(null, Font.BOLD, 30));
        }
    }

    PausePanel() {
        pauseText = new JLabel("Game");
        pauseText.setForeground(Color.WHITE);
        pauseText.setPreferredSize(new Dimension(200, 240));
        pauseText.setFont(new Font(null, Font.BOLD, 30));
        pauseText.setHorizontalAlignment(JLabel.CENTER);
        pauseText.setVerticalAlignment(JLabel.CENTER);

        setBackground(new Color(0, 0, 0, 0));

        add(pauseText);
    }
}
