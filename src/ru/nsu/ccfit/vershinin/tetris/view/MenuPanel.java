package ru.nsu.ccfit.vershinin.tetris.view;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

class MenuPanel extends JPanel {

    private JButton newGameButton;
    private JButton highScoresButton;
    private JButton aboutButton;
    private JButton exitButton;

    void registerController(ActionListener actionListener) {
        newGameButton.addActionListener(actionListener);
        highScoresButton.addActionListener(actionListener);
        aboutButton.addActionListener(actionListener);
        exitButton.addActionListener(actionListener);
    }

    MenuPanel() {
        setLayout(null);

        newGameButton = new JButton("New Game");

        newGameButton.setActionCommand("new game");

        newGameButton.setBounds(200, 130, 200, 30);

        add(newGameButton);

        highScoresButton = new JButton("High Scores");

        highScoresButton.setActionCommand("high scores");

        highScoresButton.setBounds(200, 360, 200, 30);

        add(highScoresButton);

        aboutButton = new JButton("About");

        aboutButton.setActionCommand("about");

        aboutButton.setBounds(200, 590, 200, 30);

        add(aboutButton);

        exitButton = new JButton("Exit");

        exitButton.setActionCommand("exit");

        exitButton.setBounds(200, 820, 200, 30);

        add(exitButton);

        setPreferredSize(new Dimension(600, 960));

        setBackground(new Color(0, 0, 0, 0));

    }
}
