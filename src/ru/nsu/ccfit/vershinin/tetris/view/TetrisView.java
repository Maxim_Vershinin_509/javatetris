package ru.nsu.ccfit.vershinin.tetris.view;

import ru.nsu.ccfit.vershinin.tetris.controller.ControllerInterface;
import ru.nsu.ccfit.vershinin.tetris.model.ModelGetterInfoInterface;
import ru.nsu.ccfit.vershinin.tetris.observer.Observer;
import ru.nsu.ccfit.vershinin.tetris.model.TetrisModelInterface;
import ru.nsu.ccfit.vershinin.tetris.observer.Observable;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Map;
import java.util.TreeMap;

public class TetrisView implements Observer, ActionListener, KeyListener {

    private Map<Observable.SelectorPanel, Runnable> selectorTable;
    private ModelGetterInfoInterface tetrisModel;
    private ControllerInterface controller;
    private Map<String, Runnable> commandMap = new TreeMap<>();
    private TetrisFrame tetrisFrame;

    private void initSelectorTable() {
        selectorTable = new TreeMap<>();
        selectorTable.put(Observable.SelectorPanel.GAME_PANEL, () -> tetrisFrame.updateGamePanel(tetrisModel.getField()));
        selectorTable.put(Observable.SelectorPanel.NEXT_FIGURE_PANEL, () -> tetrisFrame.updateNextFigurePanel(tetrisModel.getCoordNextFigure(), tetrisModel.getColorNextFigure()));
        selectorTable.put(Observable.SelectorPanel.LEVEL_PANEL, () -> tetrisFrame.updateLevelPanel(tetrisModel.getLevel()));
        selectorTable.put(Observable.SelectorPanel.PAUSE_PANEL, () -> tetrisFrame.updatePausePanel(tetrisModel.isPaused()));
        selectorTable.put(Observable.SelectorPanel.SCORE_PANEL, () -> tetrisFrame.updateScorePanel(tetrisModel.getScore()));
        selectorTable.put(Observable.SelectorPanel.END_GAME_DIALOG, () ->  tetrisFrame.updateEndGameDialog(tetrisModel.isRegisterRecord()));
        selectorTable.put(Observable.SelectorPanel.NEW_GAME_PANEL, () -> tetrisFrame.initGamePanels());
    }

    private void showHighScores() {
        tetrisFrame.showHighScores(tetrisModel.getScoresTable());
    }

    private void exit() {
        tetrisFrame.dispose();
    }

    private void showAbout() {
        tetrisFrame.showAbout();
    }

    private void initCommandMap() {
        commandMap.put("new game", () -> controller.startGame());
        commandMap.put("about",  this::showAbout);
        commandMap.put("high scores", this::showHighScores);
        commandMap.put("exit", this::exit);
        commandMap.put("enter name", () -> controller.setRecords());
    }

    public String getPlayerName() {
        return tetrisFrame.getEnteredName();
    }

    public void goToMenu() {
        tetrisFrame.goToMenu();
    }

    @Override
    public void update(Observable.SelectorPanel selectorPanel) {
        selectorTable.get(selectorPanel).run();
    }

    @Override
    public void keyReleased(KeyEvent keyEvent) {

    }

    @Override
    public void keyTyped(KeyEvent keyEvent) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_P) {
            controller.setPaused();
        } else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
            controller.down();
        } else if (e.getKeyCode() == KeyEvent.VK_UP) {
            controller.rotate();
        } else if (e.getKeyCode() == KeyEvent.VK_LEFT) {
            controller.moveLeft();
        } else if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
            controller.moveRight();
        } else if (e.getKeyCode() == KeyEvent.VK_SPACE) {
            controller.fall();
        } else if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
            controller.goToMenu();
        }
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        String command = actionEvent.getActionCommand();
        commandMap.get(command).run();
    }

    public TetrisView(ControllerInterface c, TetrisModelInterface t) {
        tetrisFrame = new TetrisFrame(this);
        tetrisModel = t;
        controller = c;
        tetrisModel.registerObserver(this);
        initCommandMap();
        initSelectorTable();
        tetrisFrame.setVisible(true);
    }

}
