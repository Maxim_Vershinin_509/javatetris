package ru.nsu.ccfit.vershinin.tetris.view;

import javax.swing.*;
import java.awt.*;

class InfoAboutGamePanel extends JPanel {
    private LevelPanel levelPanel = new LevelPanel();
    private NextFigurePanel nextFigurePanel = new NextFigurePanel();
    private PausePanel pausePanel = new PausePanel();
    private ScorePanel scorePanel = new ScorePanel();


    void setNextFigurePanel(int[][] nextFigure, Color colorNextFigure) {
        this.nextFigurePanel.setNextFigure(nextFigure);
        this.nextFigurePanel.setColorNextFigure(colorNextFigure);
    }

    void setScorePanel(int score) {
        this.scorePanel.setScore(score);
    }

    void setLevelPanel(int level) {
        levelPanel.setLevelText(level);
    }

    void setPausePanel(boolean isPaused) {
        pausePanel.setPauseText(isPaused);
    }

    InfoAboutGamePanel() {
        setLayout(null);

        pausePanel.setBounds(0, 0, 200, 240);

        add(pausePanel);

        nextFigurePanel.setBounds(0, 240, 200, 240);

        add(nextFigurePanel);

        scorePanel.setBounds(0, 480, 200, 240);

        add(scorePanel);

        levelPanel.setBounds(0, 720, 200, 240);

        add(levelPanel);

        setBackground(new Color(0, 0, 0, 0));

        setPreferredSize(new Dimension(200, 960));
    }
}
